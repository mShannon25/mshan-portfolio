import { Component, AfterViewInit, OnChanges, ComponentFactoryResolver, ViewContainerRef, ViewChild, Renderer, OnDestroy } from '@angular/core';
import { NgClass } from '@angular/common';
import { Router } from "@angular/router";
//import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements AfterViewInit {
  // static componentFactory = null;
  // static containerRef = null;
  static deviceIsTouch = null;
  static mobileMenuDarkenStyles = false;
  private darkenBg = {'background-darken':false};
  // static vcr = null;
  // static cfr = null;
  // static mm = null;
  // static render = null;

  //@ViewChild('mobileMenu', { read: ViewContainerRef }) mobileMenu: ViewContainerRef;

  constructor(
    // private viewContainerRef: ViewContainerRef,
    // private componentFactoryResolver: ComponentFactoryResolver,
    // private renderer: Renderer
  ) {
    // AppComponent.vcr = viewContainerRef;
    // AppComponent.cfr = componentFactoryResolver;
    // AppComponent.render = renderer;
  }

  ngAfterViewInit() {
    // const componentFactory = this.componentFactoryResolver.resolveComponentFactory(MobileMenuComponent);
    // AppComponent.mm = this.mobileMenu;
    AppComponent.deviceIsTouch = ('ontouchstart' in document.documentElement);
    AppComponent.mobileMenuDarkenStyles = false;
    this.darkenBg = {'background-darken':false};
    console.log(this.darkenBg);
    //  alert(AppComponent.deviceIsMobile);
    //   const dyynamicComponent = <MobileMenuComponent>this.mobileMenu.createComponent(componentFactory).instance;
    //  dyynamicComponent.data = 'Hello World';
    //   console.log("Mobile Nav View "+dyynamicComponent.data);
  }
  public static updateMoMenu(boo) {
     AppComponent.mobileMenuDarkenStyles = boo;
  }
  public static addMobileMenu() {
    // const componentFactory = AppComponent.cfr.resolveComponentFactory(MobileMenuComponent);
    // AppComponent.mm.clear();
    // const dyynamicComponent = <MobileMenuComponent>AppComponent.mm.createComponent(componentFactory).instance;
    // dyynamicComponent.data = 'Passing Data Test';

    // // Setup for remove
    // let closeMM = AppComponent.render.listen(dyynamicComponent.moMenuCloseBtn.nativeElement, 'click', (evt) => {
    //   // console.log('Clicking the button', evt);
    //   AppComponent.removeMobileMenu();
    // });

  }
  // public static removeMobileMenu() {
  //   const dyynamicComponent = AppComponent.mm.clear();
  // }
}
