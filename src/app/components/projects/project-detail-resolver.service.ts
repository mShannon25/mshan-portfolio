// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/take';
// import { Injectable }             from '@angular/core';
// import { Observable }             from 'rxjs/Observable';
// import { Router, Resolve, RouterStateSnapshot,
//          ActivatedRouteSnapshot } from '@angular/router';

// import { ProjectService }  from '../../services/projects.service';

// @Injectable()
// export class ProjectDetailResolver implements Resolve<any> {
//   constructor(private cs: ProjectService, private router: Router) {}

//   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
//     let id = route.paramMap.get('id');

//     return this.cs.getProject(id).take(1).map(projects => {
//       if (projects) {
//         return projects;
//       } else { // id not found
//        // this.router.navigate(['/crisis-center']);
//         return null;
//       }
//     });
//   }
// }
