import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectsComponent }    from './projects.component';
import { ProjectDetailComponent }  from './project-detail/project-detail.component';

const projectRoutes: Routes = [
  // { path: 'projects', redirectTo: '/projects' },
  // { path: 'project-detail/:id', redirectTo: './project-detail/:id' },
  { path: 'project',  component: ProjectsComponent },
  { path: 'project-detail/:id', component: ProjectDetailComponent }
    
];

@NgModule({
  imports: [
    RouterModule.forChild(projectRoutes)
   // RouterModule.forRoot(projectRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProjectRoutingModule { }