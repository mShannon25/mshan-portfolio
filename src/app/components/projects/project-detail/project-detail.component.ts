import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
	selector: 'project-detail',
	templateUrl: './project-detail.component.html',
	styleUrls: ['./project-detail.component.css']
})

export class ProjectDetailComponent implements OnInit {
	public id: number;
	public name: string;
	public type = null;
	public url = null;
	public images = null;
	public icon = null;
	
	constructor(private route: ActivatedRoute) {

	}
	ngOnInit() {
		this.route
        .queryParams
        .subscribe(params => {
			this.id = params['id'];
            this.name = params['name'];
			this.type = params['type'];
			this.url = params['url'];
			this.images = params['image'];
			this.icon = params['icon'];
			console.log("Name "+this.name);
			console.log("Type "+this.type);
			console.log("Url "+this.url);
			console.log("Images "+this.images);
		//	console.log("Name "+this.name+" Type "+this.type);
        });
	}
	test() {
		console.log("GO BACK")
	}
}