import { TestBed, inject } from '@angular/core/testing';

import { ProjectDetailComponent } from './project-detail.component';

describe('a project-detail component', () => {
	let component: ProjectDetailComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				ProjectDetailComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([ProjectDetailComponent], (ProjectDetailComponent) => {
		component = ProjectDetailComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});