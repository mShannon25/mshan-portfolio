export class Project {
    id: number;
    name: string;
    role: string;
    type: string;
    platform: string;
    tech: string;
    overview: string;
    status: string;
    images: object;
    icon: string;
}