import { Component, OnInit } from '@angular/core';
import { TweenLite } from "gsap";
import { Project } from "./project";
import { ProjectService } from "../../services/projects.service";

import { Router, NavigationExtras } from '@angular/router';
import { AppComponent } from '../../app.component';


@Component({
	selector: 'projects',
	templateUrl: 'projects.component.html',
	styleUrls: ['projects.component.css']
})

export class ProjectsComponent implements OnInit {
	public imageDir: string = "./assets/proj-images/";
	public projects = new Array();
	private _screenSize = window.innerWidth;
	private _isTouchDevice = AppComponent.deviceIsTouch;

	constructor(
		private projectService: ProjectService,
		private route: Router
	) { };

	ngOnInit() {
		console.log("Init Projects")
		// add .subscribe to get a return
		this.projects = this.projectService.getProjects();
		console.log(this.projects);

		this.onTouchDevice();
	}
	private onTouchDevice(): boolean {
		console.log("on Touch Device? " + this._isTouchDevice);
		return this._isTouchDevice;
	}
	private selectProject(projId) {
		console.log(projId);
		//<a [routerLink]="['/hero', hero.id]">
		const data = this.projectService.getProject(projId + 1);
		let img = null;
		// while(Array(data.images).length > 0) {
		// 	let pop = Array(data.images).pop();
		// 	img += pop;
		// }
		//console.log("image string "+ data.images);
		let navigationExtras: NavigationExtras = {
			queryParams: { 'id': data.id, "type": data.type, "name": data.name, "url": this.imageDir, "image": data.images, "icon": data.icon },
			fragment: 'anchor'
		};

		//console.log(data.images);
		this.route.navigate(['project-detail', projId], navigationExtras);
	}
	private overProject(e) {
		const projImage = e.target.children[0].children[0];
		const projInfo = e.target.children[1];
		if (!this._isTouchDevice) {
			let fadeBgIn = TweenLite.to(projImage, .5, {
				opacity: .4
			})
			let scaleUpBgImg = TweenLite.to(projImage, 2, {
				scaleX: 1.10,
				scaleY: 1.10
			})
			let fadeInfoIn = TweenLite.to(projInfo, .5, {
				opacity: .9
			})
			projImage.style.cursor = "pointer";
		}
	}
	public outProject(e) {
		if (!this._isTouchDevice) {
			const projImage = e.target.children[0].children[0];
			const projInfo = e.target.children[1];
			let fadeBgOut = TweenLite.to(projImage, .25, {
				opacity: 1
			});
			let scaleDownBgImg = TweenLite.to(projImage, .5, {
				scaleX: 1,
				scaleY: 1
			})
			let fadeInfoOut = TweenLite.to(projInfo, .25, {
				opacity: 0
			});
			projImage.style.cursor = "auto";
		}
	}

	private growBgImage(img) {

	}
	screenIsMobile() {
		if (window.innerWidth <= 480) {
			return true;
		}
		return false;
	}

	// _fadeImage(img, dir, val){
	// 	//let image = img;
	// 	if(dir==="out"){
	// 		img.style.opacity = 0;
	// 		img.style.webkitTransition  = 'opacity 1s easeOut';// Chrome
	// 		img.style.transition  = 'opacity 1s easeOut';// Mozilla
	// 	}else{
	// 		img.style.opacity = 1;
	// 		img.style.webkitTransition  = 'opacity 3s easeOut';// Chrome
	// 		img.style.transition  = 'opacity 3s easeOut';// Mozilla
	// 	}
	// }
}