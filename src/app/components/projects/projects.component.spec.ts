import { TestBed, inject } from '@angular/core/testing';

import { ProjectsComponent } from './projects.component';

describe('a projects component', () => {
	let component: ProjectsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				ProjectsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([ProjectsComponent], (ProjectsComponent) => {
		component = ProjectsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});