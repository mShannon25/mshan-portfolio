import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { MobileMenuComponent } from './mobile-menu.component';
import {MenuComponent} from '../menu/menu.component';

// import { ProjectRoutingModule } from './project-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MenuComponent
    // ProjectRoutingModule
  ],
  declarations: [
    MobileMenuComponent
  ],
  providers: []
})
export class MobileMenuModule {}