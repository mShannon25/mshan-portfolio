import { NgModule } from '@angular/core';
 import { RouterModule, Routes } from '@angular/router';

 import { ProjectsComponent } from './components/projects/projects.component';
 import { AboutComponent }    from './components/about/about.component';
 import { ContactComponent }  from './components/contact/contact.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'projects', pathMatch: 'full' },
    {
        path: 'projects',
        component: ProjectsComponent
    },  
    {
        path: 'about',
        component: AboutComponent
    },    
    {
        path: 'contact',
        component: ContactComponent
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
                useHash:true
            }
        )
    ],
    exports: [
         RouterModule
    ]
})

export class AppRoutingModule { }