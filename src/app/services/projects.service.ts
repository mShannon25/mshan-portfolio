import { Injectable } from "@angular/core";
import { Project } from "../components/projects/project";
// import 'rxjs/add/observable/of';
// import 'rxjs/add/operator/map';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ProjectService {
    public projects = new Array<Project>(
        { id: 1, name: "HomeByn", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["Edwards/IMG_0089.PNG", "Edwards/IMG_0091.PNG","Edwards/IMG_0090.PNG"], icon:"/assets/icons/homebyn-logo@2x.png" },
        { id: 2, name: "Edwards Life Sciences", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["civic-2015/civic_design_01.jpg","Edwards/IMG_0087.PNG","Edwards/IMG_0091.PNG","civic-2015/civic_design_01.jpg"], icon:"/assets/icons/EdwardsLogo.png"},
        { id: 3, name: "Edwards Native App", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["Edwards/IMG_0087.PNG", "Edwards/IMG_0091.PNG","civic-2015/civic_design_01.jpg"], icon:"/assets/icons/homebyn-logo@2x.png" },
        { id: 4, name: "Honda CRV", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["Edwards/IMG_0087.PNG", "Edwards/IMG_0091.PNG","Edwards/IMG_0092.PNG"], icon:"/assets/icons/homebyn-logo@2x.png" },
        { id: 5, name: "Acura Selling", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["Edwards/IMG_0089.PNG"], icon:"/assets/icons/homebyn-logo@2x.png" },
        { id: 6, name: "Ter Stegen", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["Edwards/IMG_0089.PNG", "Edwards/IMG_0091.PNG","Edwards/IMG_0090.PNG"], icon:"/assets/icons/homebyn-logo@2x.png" },
        { id: 7, name: "Ter Stegen", type: "Native App", role: "Developer", platform: "Android and iOS", tech: "Angular, Javascript, HTML", overview: "blah blah blah", status: "Not Sure", images: ["civic-2015/civic_design_01.jpg", "civic-2015/civic_design_02.jpg"], icon:"/assets/icons/homebyn-logo@2x.png"}
    );
    
    public getProjects(): Project[] {
        return this.projects;
    }

    public getProject(id: number | string): Project {
        return this.projects.filter(project => project.id === id)[0];
   //    return this.getProjects().map(proj => proj.find(proj =>proj.id === +id));
    }
}  