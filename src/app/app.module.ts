import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { Router } from '@angular/router';
import { ProjectService } from "./services/projects.service";
/* NG Material2 */
// import { 
//   MatButtonModule, 
//   MatCardModule, 
//   MatToolbarModule, 
//   MatMenuModule,
//   MatIconModule
// } from '@angular/material';

import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ProjectsModule } from './components/projects/projects.module';
import { MobileMenuModule } from './components/mobile-menu/mobile-menu.module';

import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectDetailComponent } from './components/projects/project-detail/project-detail.component';
import { MenuComponent } from './components/menu/menu.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { MenuModule } from './components/menu/menu.module';
import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AboutComponent,
    ContactComponent,
    MobileMenuComponent,
    PageNotFoundComponent
  ],
  entryComponents: [MobileMenuComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ProjectsModule,
    MenuModule,
    Angular2FontawesomeModule
  ],
  providers: [ProjectService],
  bootstrap: [AppComponent]
})

export class AppModule {
  // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
}
